# distro-deps.sh
# installs dependencies using some popular package managers

set -ex

# Pass in some value that keys into the package manager commands needed.
# Think distro, package manager, older package manager, your custom system, etc.
camp=$1

if [[ ":$PATH:" != *":$HOME/.local/bin:"* ]]
then
    echo ~/.local/bin is not in your PATH. You are having a bad day.
    echo Maybe do this:
    echo export PATH=~/.local/bin:\$PATH
    exit
fi

if [[ "$camp" == "deb" ]]; then

    # this works for me
    sudo apt-get install build-essential nasm git python3-venv python3-pip python3-dev pkg-config libmount-dev flex bison
    # seem to need these for https://gitlab.freedesktop.org/gstreamer/gst-examples.git
    # sudo apt install cmake libglib2.0-dev
    # sudo apt install json-glib, libsoup, libnice, gstreamer1.0-nice

    # Maybe this isn't needed?
    # sudo apt-get build-dep gstreamer1.0-plugins-{base,good,bad,ugly}

elif [[ "$camp" == "rh" ]]; then
    # this probably does not work.  someone fix peas
    sudo yumy install build-essential nasm git python3-venv python3-pip python3-dev

    # Maybe this isn't needed?
    # sudo yuma build-dep gstreamer1.0-plugins-{base,good,bad,ugly}

elif [[ "$camp" == "dnf" ]]; then
    sudo dnf install \
        gcc gcc-c++ automake autoconf libtool gettext-devel make cmake bison flex nasm pkgconfig curl intltool rpm-build redhat-rpm-config python3-devel libXrender-devel pulseaudio-libs-devel libXv-devel mesa-libGL-devel libXcomposite-devel perl-ExtUtils-MakeMaker libXi-devel perl-XML-Simple gperf wget libXrandr-devel libXtst-devel git xorg-x11-util-macros mesa-libEGL-devel ccache openssl-devel
    # Maybe this isn't needed?
    # sudo dnf builddep gstreamer1.0-plugins-{base,good,bad,ugly}

    # https://gitlab.freedesktop.org/gstreamer/gst-ci/-/blob/master/docker/fedora/prepare.sh

elif [[ "$camp" == "archlinux" ]]; then
    pacman eat all the ghosts

elif [[ "$camp" == "brew" ]]; then
    # Applie Home Brew
    brew a cuppa

elif [[ "$camp" == "coco" ]]; then
    # https://chocolatey.org  The Package Manager for Windows - Modern Software Automation
    choco all that stuff

elif [[ "$camp" == "none" ]]; then
    echo I did this already.

else
    echo Your camp is not suported yet. Please fix.
    exit
fi

python3 -m pip install --user meson ninja
git clone https://gitlab.freedesktop.org/gstreamer/gst-build
cd gst-build/
meson builddir
ninja -C builddir

builddir/subprojects/gstreamer/tools/gst-inspect-1.0 --version
echo Entering development environment shell
ninja -C builddir devenv
